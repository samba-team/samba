Source: samba
Section: net
Priority: optional
Maintainer: Debian Samba Maintainers <pkg-samba-maint@lists.alioth.debian.org>
Uploaders: Jelmer Vernooĳ <jelmer@debian.org>,
           Mathieu Parent <sathieu@debian.org>,
           Michael Tokarev <mjt@tls.msk.ru>
Homepage: https://www.samba.org
Standards-Version: 4.7.0
Build-Depends:
	debhelper-compat (= 13),
	dh-exec,
Build-Depends-Arch:
	dh-sequence-python3,
        python3-setuptools,
	dpkg-dev (>= 1.22.5) <!pkg.samba.before-trixie>,
# tools:
	bison,
	docbook-xml, docbook-xsl, xsltproc,
	flex,
	perl:any,
# :native for bookworm, on trixie and up it is not needed
	libparse-yapp-perl:native | libparse-yapp-perl,
	rpcsvc-proto  <!pkg.samba.before-trixie>,
# for winexe. gcc-mingw-w64 brings 4 toolchains, we need only 2
        gcc-mingw-w64-i686-win32, gcc-mingw-w64-x86-64-win32,
# system libraries:
	pkgconf,
	libacl1-dev,
	libarchive-dev,
	libavahi-client-dev,
	libavahi-common-dev,
	libblkid-dev,
	libbsd-dev,
	libcap-dev [linux-any],
# the same [arch list] is in ctdb.install
	libcephfs-dev [amd64 arm64 mips64el ppc64el riscv64 s390x],
	librados-dev  [amd64 arm64 mips64el ppc64el riscv64 s390x],
	libcmocka-dev,
	libcups2-dev,
	libdbus-1-dev,
	libglusterfs-dev [amd64 arm64 loong64 mips64el ppc64 ppc64el riscv64 s390x sparc64],
	libgnutls28-dev,
	libgpgme11-dev,
	libicu-dev,
	libjansson-dev,
	libkeyutils-dev [linux-any],
	libkrb5-dev (>= 1.21.0~) <pkg.samba.mitkrb5>,
	libldap2-dev,
	liblmdb-dev,
	libpam0g-dev,
	libpcap-dev [hurd-any],
	libpopt-dev,
	libreadline-dev,
	libtirpc-dev,
	libsystemd-dev [linux-any],
	libtasn1-6-dev,
	libtasn1-bin,
	liburing-dev [linux-any],
# xfslibs-dev is not multi-arch-usable but we only need .h (#1092689)
	xfslibs-dev:native [linux-any],
	zlib1g-dev,
# python (+#904999):
	python3-dev:native, libpython3-dev,
# :native for bookworm, for trixie and up it is not needed
	python3-dnspython:native | python3-dnspython,
	python3-etcd:native | python3-etcd,
	python3-markdown:native | python3-markdown,
# dependencies needed for selftest:
#	python3-testtools <!nocheck>,
#	lmdb-utils <!nocheck>,
#	openssl <!nocheck>,
#	python3-cryptography <!nocheck>,
#	python3-iso8601 <!nocheck>,
#	python3-pyasn1 <!nocheck>,
#	tdb-tools <!nocheck>,
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/samba-team/samba
Vcs-Git: https://salsa.debian.org/samba-team/samba.git

Package: samba
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: passwd,
         procps,
         samba-common (= ${source:Version}),
         samba-common-bin (=${binary:Version}),
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Recommends: attr,
            python3-samba,
# samba-ad-dc has been split out of samba in 4.20.1+dfsg-2.  Keep it
# in Recommends for one release cycle to avoid breaking existing installs
            samba-ad-dc,
Suggests: ctdb,
          ufw,
          winbind,
          samba-vfs-ceph, samba-vfs-glusterfs,
# move libdfs-server-ad-samba4.so.0 samba-libs=>samba-vfs-modules in 4.19.0~rc1
# swallow samba-vfs-modules by samba in 4.20.2-dfsg-3
Replaces: samba-libs (<< 2:4.19.0~),
          samba-vfs-modules (<< 2:4.20.2+dfsg-3~),
Breaks:   samba-ad-provision (<< ${source:Upstream-Version}),
          samba-libs (<< 2:4.19.0~),
          samba-vfs-modules (<< 2:4.20.2+dfsg-3~),
          samba-ad-dc (<< 2:4.20.1+dfsg-2~),
Description: SMB/CIFS file, print, and login server for Unix
 Samba is an implementation of the SMB/CIFS protocol for Unix systems,
 providing support for cross-platform file and printer sharing with
 Microsoft Windows, OS X, and other Unix systems.  Samba can also function
 as an Active Directory or NT4-style domain controller, and can integrate
 with Active Directory realms or NT4 domains as a member server.
 .
 This package provides the components necessary to use Samba as a stand-alone
 file and print server or as an NT4 domain controller.  For use in an NT4
 domain or Active Directory realm, you will also need the winbind package.
 To use samba as an Active Directory domain controller (AD DC), please install
 samba-ad-dc package.
 .
 This package is not required for connecting to existing SMB/CIFS servers
 (see smbclient) or for mounting remote filesystems (see cifs-utils).

Package: samba-libs
Pre-Depends: ${misc:Pre-Depends}
Multi-Arch: same
Architecture: any
Section: libs
Provides: libndr6 (= ${binary:Version}),
# libndr5=>6 dropped 3 long-unused KRB5_EDATA_NTSTATUS symbols
 libndr5 (= ${binary:Version}),
 libsmbldap2 (= ${binary:Version})
Depends: ${misc:Depends}, ${shlibs:Depends},
# since libldb ABI is incorrectly versioned resulting in breakage like #1021371,
# just require libldb version of the same build
# https://lists.samba.org/archive/samba-technical/2023-September/138422.html
 libldb2 (= ${ldb:Version}),
# other libs built from samba sources (it'd be nice to have dpkg-shlibdeps generate these).
# this helps to overcome the wrong version mishap (4.21.3+dfsg-5), can be dropped for trixie
 libtalloc2 (>= ${talloc:Version}),
 libtevent0t64 (>= ${tevent:Version}) <!pkg.samba.before-trixie>,
 libtevent0    (>= ${tevent:Version}) < pkg.samba.before-trixie>,
 libtdb1 (>= ${tdb:Version}),
Replaces:
# libsamba-util.so &deps moved from libwbclient0 to samba-libs in 4.16.1+dfsg-7
 libwbclient0 (<< 2:4.16.1+dfsg-7~),
# libpac-samba4.so.0 moved from samba to samba-libs in 4.17.0+dfsg-2
 samba (<< 2:4.17.0+dfsg-2~),
Breaks:
 libwbclient0 (<< 2:4.16.1+dfsg-7~),
 samba (<< 2:4.17.0+dfsg-2~),
 sssd-ad (<< 2.9.4-1+b1),
 sssd-ad-common (<< 2.9.4-1+b1),
 sssd-ipa (<< 2.9.4-1+b1),
Description: Samba core libraries
 Samba is an implementation of the SMB/CIFS protocol for Unix systems,
 providing support for cross-platform file sharing with Microsoft Windows, OS X,
 and other Unix systems.  Samba can also function as a domain controller
 or member server in Active Directory or NT4-style domains.
 .
 This package contains the shared libraries.

Package: samba-common
Architecture: all
Multi-Arch: foreign
Pre-Depends: ${misc:Pre-Depends}
Depends: ucf, ${misc:Depends}
Recommends: samba-common-bin
Description: common files used by both the Samba server and client
 Samba is an implementation of the SMB/CIFS protocol for Unix systems,
 providing support for cross-platform file and printer sharing with
 Microsoft Windows, OS X, and other Unix systems.
 .
 This package contains common files used by all parts of Samba.

Package: samba-common-bin
Architecture: any
Depends: samba-common (= ${source:Version}),
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Recommends: python3-samba,
Suggests: heimdal-clients,
# moved libnet-keytab-samba4.so.0 & libRPC-WORKER-samba4.so.0 samba-libs=>samba-common-bin in 4.19.0~r1
# moved smbcontrol samba=>samba-common-bin in 4.20.0-2
# in kali linux, winexe initially has been in its own pkg (#1081269)
Replaces: samba (<< 2:4.20.1+dfsg-2~), samba-libs (<< 2:4.19.0~), winexe (<< 2:4.21.1+dfsg-3~)
Breaks:   samba (<< 2:4.20.1+dfsg-2~), samba-libs (<< 2:4.19.0~), winexe (<< 2:4.21.1+dfsg-3~)
Provides: winexe (= ${binary:Version})
Description: Samba common files used by both the server and the client
 Samba is an implementation of the SMB/CIFS protocol for Unix systems,
 providing support for cross-platform file sharing with Microsoft Windows, OS X,
 and other Unix systems.  Samba can also function as a domain controller
 or member server in Active Directory or NT4-style domains.
 .
 This package contains the common files that are used by both the server
 (provided in the samba package) and the client (provided in the smbclient
 package).

Package: samba-ad-dc
Architecture: any
Multi-Arch: foreign
Pre-Depends: ${misc:Pre-Depends}
Depends: samba (= ${binary:Version}),
	 python3-samba (= ${binary:Version}),
	 python3-dnspython,
	 samba-dsdb-modules (= ${binary:Version}),
	 winbind (= ${binary:Version}),
	 krb5-kdc (>= 1.21.0~) <pkg.samba.mitkrb5>,
	 ${python3:Depends},
	 ${shlibs:Depends},
	 ${misc:Depends}
Recommends: libnss-winbind, libpam-winbind,
# samba-ad-provision is needed for setup only
	samba-ad-provision,
# used in a few AD-related subcommands of samba-tool (see #876984)
        python3-gpg,
Suggests: bind9,
	  bind9utils,
	  ldb-tools,
# Besides wanting accurate time, samba DC can provide signed time source
# to the domain using special socket (see "ntp signd socket directory"
# in smb.conf(5)).  There was a (wrong) patch to ntp to work with samba,
# it has been dropped in ntpsec.
	  time-daemon | chrony | ntpsec,
Enhances: bind9, ntpsec
Breaks:   samba-ad-provision (<< ${source:Upstream-Version}),
# files moved from samba & samba-common-bin in 4.20.1-2:
	  samba-common-bin (<< 2:4.20.1+dfsg-2~),
Replaces: samba-common-bin (<< 2:4.20.1+dfsg-2~),
# Breaks+Replaces upgraded to Conflicts for DEP17 + /usr-move
Conflicts: samba (<< 2:4.20.1+dfsg-2~),
Description: Samba control files to run AD Domain Controller
 Samba is an implementation of the SMB/CIFS protocol for Unix systems,
 providing support for cross-platform file and printer sharing with
 Microsoft Windows, OS X, and other Unix systems.
 .
 This package contains control files to run an Active Directory Domain
 Controller (AD DC).  For now, this is just a metapackage pulling in
 all the required dependencies.

Package: samba-ad-provision
Architecture: all
Multi-Arch: foreign
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends},
 python3-markdown,
Replaces: samba (<< 2:4.17.3+dfsg-4~)
Breaks:   samba (<< 2:4.17.3+dfsg-4~)
Description: Samba files needed for AD domain provision
 Samba is an implementation of the SMB/CIFS protocol for Unix systems,
 providing support for cross-platform file and printer sharing with
 Microsoft Windows, OS X, and other Unix systems.
 .
 This package contains files to setup an Active Directory Domain
 Controller (AD DC).

Package: smbclient
Architecture: any
Depends: samba-common (= ${source:Version}),
         samba-libs (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Provides: samba-client
Suggests: cifs-utils, heimdal-clients
Description: command-line SMB/CIFS clients for Unix
 Samba is an implementation of the SMB/CIFS protocol for Unix systems,
 providing support for cross-platform file and printer sharing with
 Microsoft Windows, OS X, and other Unix systems.
 .
 This package contains command-line utilities for accessing Microsoft
 Windows and Samba servers, including smbclient, smbtar, and smbspool.
 Utilities for mounting shares locally are found in the package
 cifs-utils.

Package: samba-testsuite
Architecture: any
Suggests: subunit
Depends: samba-common-bin,
         samba-libs (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
# move libshares-samba4.so.0 samba-libs=>samba-testsuite in 4.19.0~rc1
Replaces: samba-libs (<< 2:4.19.0~)
Breaks:   samba-libs (<< 2:4.19.0~)
Description: test suite from Samba
 Samba is an implementation of the SMB/CIFS protocol for Unix systems,
 providing support for cross-platform file sharing with Microsoft Windows, OS X,
 and other Unix systems.  Samba can also function as a domain controller
 or member server in Active Directory or NT4-style domains.
 .
 This package contains programs for testing the reliability and speed
 of SMB servers, Samba in particular.

Package: registry-tools
Architecture: any
Depends: samba-libs (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Description: tools for viewing and manipulating the Windows registry
 Samba is an implementation of the SMB/CIFS protocol for Unix systems,
 providing support for cross-platform file sharing with Microsoft Windows, OS X,
 and other Unix systems.  Samba can also function as a domain controller
 or member server in Active Directory or NT4-style domains.
 .
 This package contains tools for viewing and manipulating the binary
 "registry" found on Windows machines, both locally and remote.

Package: samba-dev
Architecture: any
Multi-Arch: same
Depends: libc6-dev,
         libldb-dev,
         libpopt-dev,
         libtalloc-dev,
         libtdb-dev,
         libtevent-dev,
         libwbclient-dev,
         samba-libs (= ${binary:Version}),
         ${misc:Depends}
# libsamba-util.so moved from libwbclient0 to samba-libs in 4.16.1+dfsg-7
Replaces: libwbclient-dev (<< 2:4.16.1+dfsg-7~)
Breaks:   libwbclient-dev (<< 2:4.16.1+dfsg-7~)
Section: devel
Description: tools for extending Samba
 Samba is an implementation of the SMB/CIFS protocol for Unix systems,
 providing support for cross-platform file sharing with Microsoft Windows, OS X,
 and other Unix systems.  Samba can also function as a domain controller
 or member server in Active Directory or NT4-style domains.
 .
 This package contains include files shared by the various Samba-based
 libraries.

Package: python3-samba
Pre-Depends: ${misc:Pre-Depends}
Architecture: any
Section: python
Depends: python3-ldb (= ${ldb:Version}),
         samba-libs (= ${binary:Version}),
         python3-tdb,
         python3-cryptography,
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Breaks:
# samba-tool & samba-gpupdate moved from samba-common-bin & samba in 4.20.1-2:
          samba-common-bin (<< 2:4.20.1+dfsg-2~), samba (<< 2:4.20.1+dfsg-2~),
Replaces:
          samba-common-bin (<< 2:4.20.1+dfsg-2~), samba (<< 2:4.20.1+dfsg-2~),
# libsamba-policy & helpers and dckeytab python libs moved to python3-samba
          samba-libs (<< 2:4.16.0+dfsg-1~),
Recommends: tdb-tools,
Description: Python 3 bindings for Samba
 Samba is an implementation of the SMB/CIFS protocol for Unix systems,
 providing support for cross-platform file sharing with Microsoft Windows, OS X,
 and other Unix systems.  Samba can also function as a domain controller
 or member server in Active Directory or NT4-style domains.
 .
 This package contains Python 3 bindings for most Samba libraries.

Package: samba-dsdb-modules
Architecture: any
Multi-Arch: same
Section: libs
Depends: samba-libs (= ${binary:Version}),
         libldb2 (= ${ldb:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Enhances: libldb2
Description: Samba Directory Services Database
 Samba is an implementation of the SMB/CIFS protocol for Unix systems,
 providing support for cross-platform file sharing with Microsoft Windows, OS X,
 and other Unix systems.  Samba can also function as a domain controller
 or member server in Active Directory or NT4-style domains.
 .
 This package contains LDB plugins which add support for various Active
 Directory features to the LDB library.

Package: samba-vfs-modules
# Remove this package for trixie+1
Architecture: any
Section: oldlibs
Depends: ${misc:Depends}
Description: Samba Virtual FileSystem plugins (transitional package)
 Samba is an implementation of the SMB/CIFS protocol for Unix systems,
 providing support for cross-platform file sharing with Microsoft Windows, OS X,
 and other Unix systems.  Samba can also function as a domain controller
 or member server in Active Directory or NT4-style domains.
 .
 Virtual FileSystem modules are stacked shared libraries extending the
 functionality of Samba.  This package used to provide VFS modules for
 samba, but since version 4.20.2+dfsg-3, most of the modules were merged
 into main samba package, or into their own separate packages -
 samba-vfs-ceph and samba-vfs-glusterfs.
 .
 This package can safely be removed.

Package: samba-vfs-ceph
Architecture: amd64 arm64 mips64el ppc64el riscv64 s390x
Depends: samba (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
# ceph modules moved from samba-vfs-modules in 4.20.2+dfsg-3
Replaces: samba-vfs-modules (<< 2:4.20.2+dfsg-3~)
Breaks:   samba-vfs-modules (<< 2:4.20.2+dfsg-3~)
Enhances: samba
Description: Samba Virtual FileSystem ceph modules
 Samba is an implementation of the SMB/CIFS protocol for Unix systems,
 providing support for cross-platform file sharing with Microsoft Windows, OS X,
 and other Unix systems.  Samba can also function as a domain controller
 or member server in Active Directory or NT4-style domains.
 .
 Virtual FileSystem modules are stacked shared libraries extending the
 functionality of Samba. This package provides vfs_ceph and vfs_ceph_snapshots
 modules.

Package: samba-vfs-glusterfs
Architecture: amd64 arm64 loong64 mips64el ppc64 ppc64el riscv64 s390x sparc64
Depends: samba (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
# glusterfs modules moved from samba-vfs-modules in 4.20.2+dfsg-3 (4.19.4+dfsg-2ubuntu1)
# in ubuntu, glusterfs was in samba-vfs-modules-extra temporarily
Replaces: samba-vfs-modules (<< 2:4.20.2+dfsg-3~), samba-vfs-modules-extra (<< 2:4.20.2+dfsg-3~)
Breaks:   samba-vfs-modules (<< 2:4.20.2+dfsg-3~), samba-vfs-modules-extra (<< 2:4.20.2+dfsg-3~)
Enhances: samba
Description: Samba Virtual FileSystem glusterfs modules
 Samba is an implementation of the SMB/CIFS protocol for Unix systems,
 providing support for cross-platform file sharing with Microsoft Windows, OS X,
 and other Unix systems.  Samba can also function as a domain controller
 or member server in Active Directory or NT4-style domains.
 .
 Virtual FileSystem modules are stacked shared libraries extending the
 functionality of Samba. This package provides vfs_glusterfs and
 vfs_glusterfs_fuse modules.

Package: libsmbclient0
# ${t64:Provides}:
Provides: libsmbclient (= ${binary:Version}) [amd64 arm64 i386 mips64el ppc64el riscv64 s390x]
Replaces: libsmbclient
Breaks: libsmbclient (<< ${source:Version})
Build-Profiles: <!pkg.samba.before-trixie>
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: samba-libs (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Description: shared library for communication with SMB/CIFS servers
 This package provides a shared library that enables client applications
 to talk to Microsoft Windows and Samba servers using the SMB/CIFS
 protocol.

Package: libsmbclient
# exactly the same as libsmbclient but for older systems before t64 transition
Build-Profiles: <pkg.samba.before-trixie>
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: samba-libs (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Description: shared library for communication with SMB/CIFS servers
 This package provides a shared library that enables client applications
 to talk to Microsoft Windows and Samba servers using the SMB/CIFS
 protocol.

Package: libsmbclient-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
 libsmbclient0 (= ${binary:Version}) <!pkg.samba.before-trixie>,
 libsmbclient  (= ${binary:Version}) < pkg.samba.before-trixie>,
# the library name is libsmbclient0 on trixie and up
Description: development files for libsmbclient
 This package provides the development files (static library and headers)
 required for building applications against libsmbclient, a library that
 enables client applications to talk to Microsoft Windows and Samba servers
 using the SMB/CIFS protocol.

Package: winbind
Pre-Depends: ${misc:Pre-Depends}
Architecture: any
Multi-Arch: allowed
Depends: samba-common (= ${source:Version}),
         samba-common-bin (=${binary:Version}),
# wbinfo (linked with libwbclient) which should use the same protocol
         libwbclient0 (=${binary:Version}),
# for groupadd in postinst
         passwd,
         ${misc:Depends},
         ${shlibs:Depends}
Enhances: libkrb5-26-heimdal <!pkg.samba.mitkrb5>
Suggests: libnss-winbind, libpam-winbind
# 4.16.6+dfsg-5 idmap_{script,rfc2307}.8 moved samba{,-libs} => winbind
Breaks:   samba (<< 2:4.16.6+dfsg-5~), samba-libs (<< 2:4.16.6+dfsg-5~),
# libpam-winbind & libnss-winbind depend on exact version since 4.16.1+dfsg-7
 libpam-winbind (<< 2:4.16.1+dfsg-7~), libnss-winbind (<< 2:4.16.1+dfsg-7~),
Replaces: samba (<< 2:4.16.6+dfsg-5~), samba-libs (<< 2:4.16.6+dfsg-5~),
Description: service to resolve user and group information from Windows NT servers
 Samba is an implementation of the SMB/CIFS protocol for Unix systems,
 providing support for cross-platform file sharing with Microsoft Windows, OS X,
 and other Unix systems.  Samba can also function as a domain controller
 or member server in Active Directory or NT4-style domains.
 .
 This package provides winbindd, a daemon which integrates authentication
 and directory service (user/group lookup) mechanisms from a Windows
 domain on a Linux system.
 .
 Winbind based user/group lookups via /etc/nsswitch.conf can be enabled via
 the libnss-winbind package. Winbind based Windows domain authentication can
 be enabled via the libpam-winbind package.

Package: libpam-winbind
Section: admin
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: libpam-runtime,
         winbind:any (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Suggests: libnss-winbind
Description: Windows domain authentication integration plugin
 Samba is an implementation of the SMB/CIFS protocol for Unix systems,
 providing support for cross-platform file and printer sharing with
 Microsoft Windows, OS X, and other Unix systems.  Samba can also function
 as an NT4-style domain controller, and can integrate with both NT4 domains
 and Active Directory realms as a member server.
 .
 This package provides pam_winbind, a plugin that integrates with a local
 winbindd server to provide Windows domain authentication to the system.

Package: libnss-winbind
Section: admin
Architecture: any
Multi-Arch: same
Depends: winbind:any (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Suggests: libpam-winbind
Description: Samba nameservice integration plugins
 Samba is an implementation of the SMB/CIFS protocol for Unix systems,
 providing support for cross-platform file and printer sharing with
 Microsoft Windows, OS X, and other Unix systems.  Samba can also function
 as an NT4-style domain controller, and can integrate with both NT4 domains
 and Active Directory realms as a member server.
 .
 This package provides nss_winbind, a plugin that integrates
 with a local winbindd server to provide user/group name lookups to the
 system; and nss_wins, which provides hostname lookups via both the NBNS and
 NetBIOS broadcast protocols.

Package: libwbclient0
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Samba winbind client library
 Samba is an implementation of the SMB/CIFS protocol for Unix systems,
 providing support for cross-platform file and printer sharing with
 Microsoft Windows, OS X, and other Unix systems.
 .
 This package provides a library for client applications that interact
 via the winbind pipe protocol with a Samba winbind server.

Package: libwbclient-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libwbclient0 (= ${binary:Version}), ${misc:Depends}
# include/samba-4.0/core/*.h moved from samba-dev in 4.19.6+dfsg-2
Breaks:   samba-dev (<< 2:4.19.6+dfsg-2~)
Replaces: samba-dev (<< 2:4.19.6+dfsg-2~)
Description: Samba winbind client library - development files
 Samba is an implementation of the SMB/CIFS protocol for Unix systems,
 providing support for cross-platform file and printer sharing with
 Microsoft Windows, OS X, and other Unix systems.
 .
 This package provides the development files (static library and headers)
 required for building applications against libwbclient, a library for client
 applications that interact via the winbind pipe protocol with a Samba
 winbind server.

Package: ctdb
Architecture: any
Multi-Arch: foreign
Depends: iproute2 [linux-any],
         psmisc,
         samba-libs (= ${binary:Version}),
         sudo,
         tdb-tools,
         time,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: ethtool [linux-any], python3:any, python3-etcd,
Suggests: lsof,
	${rados:Depends}, samba-vfs-ceph [amd64 arm64 mips64el ppc64el riscv64 s390x],
Description: clustered database to store temporary data
 CTDB is a cluster implementation of the TDB database used by Samba and other
 projects to store temporary data. If an application is already using TDB for
 temporary data it is very easy to convert that application to be cluster aware
 and use CTDB instead.
 .
 CTDB provides the same types of functions as TDB but in a clustered fashion,
 providing a TDB-style database that spans multiple physical hosts in a cluster.
 .
 Features include:
  * CTDB provides a TDB that has consistent data and consistent locking across
    all nodes in a cluster.
  * CTDB is very fast.
  * In case of node failures, CTDB will automatically recover and repair all TDB
    databases that it manages.
  * CTDB is the core component that provides pCIFS ("parallel CIFS") with
    Samba3/4.
  * CTDB provides HA features such as node monitoring, node failover, and IP
    takeover.
  * CTDB provides a reliable messaging transport to allow applications linked
    with CTDB to communicate to other instances of the application running on
    different nodes in the cluster.
  * CTDB has pluggable transport backends. Currently implemented backends are
    TCP and Infiniband.
  * CTDB supports a system of application specific management scripts, allowing
    applications that depend on network or filesystem resources to be managed in
    a highly available manner on a cluster.

Package: libldb2
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Architecture: any
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends}
Breaks: samba-libs (<< 2:4.19.0~),
Description: LDAP-like embedded database - shared library
 ldb is a LDAP-like embedded database built on top of TDB.
 .
 It provides a fast database with an LDAP-like API designed
 to be used within an application. In some ways it can be seen as a
 intermediate solution between key-value pair databases and a real LDAP
 database.
 .
 This package contains the shared library file.

Package: ldb-tools
Section: utils
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: LDAP-like embedded database - tools
 ldb is a LDAP-like embedded database built on top of TDB.
 .
 What ldb does is provide a fast database with an LDAP-like API designed
 to be used within an application. In some ways it can be seen as a
 intermediate solution between key-value pair databases and a real LDAP
 database.
 .
 This package contains bundled test and utility binaries

Package: libldb-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libc6-dev,
         libldb2 (= ${binary:Version}),
         libtalloc-dev,
         libtevent-dev,
         libtdb-dev,
         ${misc:Depends}
Description: LDAP-like embedded database - development files
 ldb is a LDAP-like embedded database built on top of TDB.
 .
 What ldb does is provide a fast database with an LDAP-like API designed
 to be used within an application. In some ways it can be seen as a
 intermediate solution between key-value pair databases and a real LDAP
 database.
 .
 This package contains the development files.

Package: python3-ldb
Pre-Depends: ${misc:Pre-Depends}
Section: python
Architecture: any
Depends: libldb2 (= ${binary:Version}),
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Replaces: python3-samba (=2:4.21.2+dfsg-1), python3-ldb-dev
Breaks: python3-ldb-dev
Description: Python 3 bindings for LDB
 ldb is a LDAP-like embedded database built on top of TDB.
 .
 This package contains the Python 3 bindings for ldb.

###################### talloc packages
Package: libtalloc2
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Section: libs
Description: hierarchical pool based memory allocator
 A hierarchical pool based memory allocator with destructors. It uses
 reference counting to determine when memory should be freed.

Package: libtalloc-dev
Section: libdevel
Depends: libtalloc2 (=${binary:Version}), ${misc:Depends}
Architecture: any
Multi-Arch: same
Description: hierarchical pool based memory allocator - development files
 A hierarchical pool based memory allocator with destructors. It uses
 reference counting to determine when memory should be freed.
 .
 This package contains the development files.

Package: python3-talloc
Multi-Arch: same
Section: python
Architecture: any
Depends: ${misc:Depends}, ${python3:Depends}, ${shlibs:Depends}
# swallow python3-talloc-dev package in talloc 2.4.2-2,
# drop files in samba 4.21.2-3
Replaces: python3-talloc-dev
Breaks:   python3-talloc-dev
Description: hierarchical pool based memory allocator - Python3 bindings
 A hierarchical pool based memory allocator with destructors. It uses
 reference counting to determine when memory should be freed.
 .
 This package contains the Python 3 bindings.

###################### tevent packages
Package: libtevent0t64
Build-Profiles: <!pkg.samba.before-trixie>
# ${t64:Provides}:
Provides: libtevent0 (= ${binary:Version}) [amd64 arm64 i386 mips64el ppc64el riscv64 s390x]
Replaces: libtevent0
Breaks: libtevent0 (<< ${binary:Version}~)
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Section: libs
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: talloc-based event loop library - shared library
 tevent is a simple library that can handle the main event loop for an
 application. It supports three kinds of events: timed events, file
 descriptors becoming readable or writable and signals.
 .
 Talloc is used for memory management, both internally and for private
 data provided by users of the library.
 .
 This package provides the shared library file.

Package: libtevent0
# the same as libtevent0t64
Build-Profiles: <pkg.samba.before-trixie>
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Section: libs
# libtevent0t64 slipped out into mjt repository (for a short while). Drop this for trixie
Replaces: libtevent0t64 (<< 2:0.16.1) <pkg.samba.before-trixie>
Breaks:   libtevent0t64 (<< 2:0.16.1) <pkg.samba.before-trixie>
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: talloc-based event loop library - shared library
 tevent is a simple library that can handle the main event loop for an
 application. It supports three kinds of events: timed events, file
 descriptors becoming readable or writable and signals.
 .
 Talloc is used for memory management, both internally and for private
 data provided by users of the library.
 .
 This package provides the shared library file.

Package: libtevent-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libc6-dev,
         libtalloc-dev,
         libtevent0t64 (= ${binary:Version}) <!pkg.samba.before-trixie>,
         libtevent0    (= ${binary:Version}) < pkg.samba.before-trixie>,
         ${misc:Depends}
Description: talloc-based event loop library - development files
 tevent is a simple library that can handle the main event loop for an
 application. It supports three kinds of events: timed events, file
 descriptors becoming readable or writable and signals.
 .
 Talloc is used for memory management, both internally and for private
 data provided by users of the library.
 .
 This package provides the development files.

###################### tdb packages
Package: libtdb1
Pre-Depends: ${misc:Pre-Depends}
Multi-Arch: same
Architecture: any
Section: libs
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Trivial Database - shared library
 This is a simple database API. It is modelled after the structure
 of GDBM. TDB features, unlike GDBM, multiple writers support with
 appropriate locking and transactions.
 .
 This package contains the shared library file.

Package: tdb-tools
Section: utils
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Trivial Database - bundled binaries
 This is a simple database API. It is modelled after the structure
 of GDBM. TDB features, unlike GDBM, multiple writers support with
 appropriate locking and transactions.
 .
 This package contains bundled test and utility binaries

Package: libtdb-dev
Multi-Arch: same
Provides: tdb-dev
Section: libdevel
Architecture: any
Depends: libc6-dev, libtdb1 (= ${binary:Version}), ${misc:Depends}
Description: Trivial Database - development files
 This is a simple database API. It is modelled after the structure
 of GDBM. TDB features, unlike GDBM, multiple writers support with
 appropriate locking and transactions.
 .
 This package contains the development files.

Package: python3-tdb
Section: python
Architecture: any
Depends: ${misc:Depends}, ${python3:Depends}, ${shlibs:Depends}
Description: Python3 bindings for TDB
 This is a simple database API. It is modelled after the structure
 of GDBM. TDB features, unlike GDBM, multiple writers support with
 appropriate locking and transactions.
 .
 This package contains the Python3 bindings.
